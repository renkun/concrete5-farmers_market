<?php defined('C5_EXECUTE') or die(_("Access Denied."));

if (is_object($vendor)) {
  $title = 'Edit';
  $task = 'edit';
  $name = $vendor->name;
  $email = $vendor->email;
  $phone = $vendor->phone;
  $keywords = $vendor->keywords;
  $id = $vendor->id;
}
else {
  $title = 'Add';
  $task = 'add';
  $id = null;
}
?>
<form method="post" class="ccm-dashboard-content-form" action="<?php echo $view->action('edit')?>">
<div class="row">
  <div class="col-xs-12 ccm-dashboard-section-menu">
    <h2><?php if (is_object($vendor)) { echo 'Edit '.$name; } else { echo 'Add New Vendor'; } ?></h2>
    <fieldset>
      <?php echo $form->hidden('id', $id) ?>
      <div class="form-group">
        <?php echo $form->label('name', t('Name')) ?> *
        <div class="input">
          <?php echo $form->text('name', $name, array('style' => 'width: 230px')) ?>
        </div>
      </div>
      <div class="form-group">
        <?php echo $form->label('email', t('Email')) ?>
        <div class="input">
          <?php echo $form->text('email', $email, array('style' => 'width: 230px')) ?>
        </div>
      </div>
      <div class="form-group">
        <?php echo $form->label('phone', t('Phone')) ?>
        <div class="input">
          <?php echo $form->text('phone', $phone, array('style' => 'width: 230px')) ?>
        </div>
      </div>
            <div class="form-group">
        <?php echo $form->label('keywords', t('Keywords')) ?> *
        <div class="input">
          <?php echo $form->textarea('keywords', $keywords, array('style' => 'width: 500px', 'rows' => '8')) ?>
        </div>
      </div>
    </fieldset>
  </div>
</div>
<div class="ccm-dashboard-form-actions-wrapper">
  <div class="ccm-dashboard-form-actions">
    <a href="<?php echo URL::to('/dashboard/market')?>" class="btn pull-left btn-default"><?php echo t('Back')?></a>
    <button class="pull-right btn btn-success" type="submit" ><?php  echo t('Save')?></button>
  </div>
</div>
</form>