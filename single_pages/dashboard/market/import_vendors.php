<?php defined('C5_EXECUTE') or die(_("Access Denied.")); 
// Get file object if $fID is set
$f = null;
if ($fID) {
  $f = File::getById($fID);
}
?>

<style>
  .ccm-dashboard-content-form {
    margin-bottom: 30px !important;
  }
</style>

<?php if ($pending_rows): ?>
<form method="post" action="<?php echo $view->action('import')?>">
<?php echo $this->controller->token->output('import')?>
<div class="row">
  <div class="col-xs-12">
    <div class="ccm-ui" id="ccm-dashboard-result-message">
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <p>Please check the accuracy of the data below. This import will delete all existing data and replace it with this list.</p>
        <p>If this does not look correct, click <b>cancel</b> to try again. Otherwise, click <b>Import</b> to proceed.</p>
        <br/>
        <div class="alert-actions">
          <button class="btn btn-danger small" type="submit"><?php echo t('Import')?></button>
          <a class="btn btn-default small" href="<?php echo $this->url('/dashboard/market/import_vendors') ?>"><?php echo t('Cancel'); ?></a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <table class="table">
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Keywords</th>
      </tr>
      <?php foreach ($pending_rows as $row) { ?>
      <tr>
      <?php
        echo sprintf("<td>%s</td>", $row['name']);
        echo sprintf("<td>%s</td>", $row['email']);
        echo sprintf("<td>%s</td>", $row['phone']);
        echo sprintf("<td>%s</td>", $row['keywords']);
      ?>
      </tr>
      <?php } ?>
    </table>
  </div>
</div>
<div class="ccm-dashboard-form-actions-wrapper">
  <div class="ccm-dashboard-form-actions">
    <a href="<?php echo URL::to('/dashboard/market/import_vendors')?>" class="btn pull-left btn-default"><?php echo t('Cancel')?></a>
    <button class="pull-right btn btn-danger" type="submit"><?php echo t('Import')?></button>
  </div>
</div>
</form>
<?php else: ?>

<form method="post" class="ccm-dashboard-content-form" action="<?php echo $view->action('process')?>">
<div class="row">
  <div class="col-xs-12">
    <h2>CSV Import</h2>
    <fieldset>
      <div class="form-group">
        <div class="col-xs-12">
          <label for="fID" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('File should be in comma or tab delimited format with four columns in the following order: name, phone, email, keywords. The selected import file will be remembered by this form until the next import.')?>"><?php echo t('Select File')?></label>
          <div class="controls">
            <?php  $al = Core::make('helper/concrete/asset_library'); ?>
            <?php  echo $al->text('fID', 'fID', 'Select File', $f); ?>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-xs-3">
          <label for="delimiter" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Delimiter is the character used to separate fields in the input file.') ?>"><?php echo t('Delimiter') ?></label>
          <select class="form-control" name="delimiter">
            <option value="comma"<?php if ($delimiter == 'comma') echo ' selected' ?>>Comma</option>
            <option value="tab"<?php if ($delimiter == 'tab') echo ' selected' ?>>Tab</option>
          </select>
        </div>
        <div class="col-xs-3">
          <label for="enclosure" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Enclosure is the character used to wrap the values in the file. Enclosure is optional and will likely be present in files where the data may include the delimiter character. For example, if the data in a comma separated file could possibly include commas then a delimiter will be used.') ?>"><?php echo t('Enclosure') ?></label>
          <select class="form-control" name="enclosure">
            <option value="none"<?php if ($enclosure == 'none') echo ' selected' ?>>None</option>
            <option value="double-quote"<?php if ($enclosure == 'double-quote') echo ' selected' ?>>"</option>
            <option value="single-quote"<?php if ($enclosure == 'single-quote') echo ' selected' ?>>'</option>
          </select>
        </div>
      </div>
    </fieldset>
  </div>
</div>
<div class="ccm-dashboard-form-actions-wrapper">
  <div class="ccm-dashboard-form-actions">
    <a href="<?php if ($pending_rows) echo URL::to('/dashboard/market/import_vendors'); else echo URL::to('/dashboard/market');?>" class="btn pull-left btn-default"><?php echo t('Cancel')?></a>
    <button class="pull-right btn btn-success" type="submit"><?php echo t('Import')?></button>
  </div>
</div>
</form>
<?php endif; ?>
