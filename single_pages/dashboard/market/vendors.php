<?php defined('C5_EXECUTE') or die(_("Access Denied."));
$th = Loader::helper('text');
echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('View/Search Vendors'),false,false,false); ?>
<style>
  .ccm-search-results-table th {
    padding: 10px 20px;
    font-weight: 600;
  }
  .ccm-search-results-table td {
    min-width: 180px;
  }
  .ccm-search-results-table td:first-child {
    min-width: 0;
    width: 80px;
  }
  .ccm-dashboard-content-full > h5 {
    text-transform: uppercase;
    color: #999;
    margin-right: 20px;
    text-align: right;
  }
</style>
<div class="ccm-dashboard-header-buttons">
  <a href="<?php echo $this->url('/dashboard/market/add_vendor')?>" class="btn btn-danger"><?php echo t('Add New Vendor')?></a>
</div>

<?php if ($remove_name) { ?>
  <div class="ccm-ui" id="ccm-dashboard-result-message">
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <p><?php echo t('Are you sure you want to delete <b>') . t($remove_name) . '</b>?'; ?></p>
        <p><?php echo t('This action can not be undone!'); ?></p>
        <br/>
        <div class="alert-actions">
          <a class="btn btn-danger small" href="<?php echo $this->url('/dashboard/market/vendors','/delete/',$remove_id,$remove_name) ?>"><?php echo t('Delete'); ?></a>
          <a class="btn btn-default small" href="<?php echo $this->url('/dashboard/market/vendors') ?>"><?php echo t('Cancel'); ?></a>
        </div>
      </div>
  </div>
<?php } ?>

<div class="ccm-dashboard-content-full">
<?php /*
  <form method="get" action="<?php   echo $this->action('view') ?>">
  Search Form Here?
  </form>
 */ ?>
  <h5><?php echo $record_count.' RECORDS' ?></h5>
  <table border="0" class="ccm-search-results-table" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <th><?php echo t('Name') ?></th>
        <th><?php echo t('Email') ?></th>
        <th><?php echo t('Phone') ?></th>
        <th><?php echo t('Keywords') ?></th>
    </tr>
    </thead>
    <?php foreach ($vendors as $vendor) { ?>
    <tr>
      <td>
        <a href="<?php echo $this->url('/dashboard/market/add_vendor','edit',$vendor->id) ?>" class="eventtooltip icon edit"><i class="fa fa-edit"></i></a> &nbsp;
        <a href="<?php echo $this->url('/dashboard/market/vendors','delete_check',$vendor->id,$vendor->name) ?>" class="eventtooltip icon delete"><i class="fa fa-trash-o"></i></a>
      </td>
      <td><?php echo $vendor->name; ?></td>
      <td><?php echo $vendor->email; ?></td>
      <td><?php echo $vendor->phone; ?></td>
      <td><?php echo $th->wordSafeShortText($vendor->keywords, 120); ?></td>
    </tr>
    <?php } ?>
  </table>
</div>