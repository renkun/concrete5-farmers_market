<?php
namespace Concrete\Package\FarmersMarket\Controller\SinglePage\Dashboard\Market;

use \Concrete\Core\Page\Controller\DashboardPageController;
use Core;
use Loader;
use File;
use Config;
use Concrete\Core\File\Importer as FileImporter;
use \Concrete\Package\FarmersMarket\Src\Models\Vendor;

class ImportVendors extends DashboardPageController {

  public $helpers = array('html', 'form');

  public function process() {
    if ($this->isPost()) {
      $fi = new FileImporter();
      // Validate input
      if (is_null($this->post('fID')) || $this->post('fID') == 0)
        $this->error->add('You must select an input file.');
      else {
        $f = File::getByID($this->post('fID'));
        if (!is_object($f)) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        }
      }

      if (is_null($this->post('delimiter')))
        $this->error->add('You must select a delimter.');
      else 
        $delimiter = $this->getDelimiter($this->post('delimiter'));
      $enclosure = $this->getEnclosure($this->post('enclosure'));

      // Check file and present contents for inspection
      if (!$this->error->has()) {
        $fsr = $f->getFileResource();
        if (!$fsr->isFile()) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        } else {
          // Parse file and set pending rows
          $rows = $this->parseFile($fsr, $delimiter, $enclosure);
          if (count($rows) == 0) {
            $this->error->add('No records found.');
          }
          else {
            $this->set('pending_rows', $rows);
            // Records the file ID and settings
            Config::save('farmmarket.last-import-id', $this->post('fID'));
            Config::save('farmmarket.last-import-delimiter', $this->post('delimiter'));
            Config::save('farmmarket.last-import-enclosure', $this->post('enclosure'));
            $this->setFormData();
          }
        }
      }
    }
  }

  public function import() {
    if ($this->token->validate("import")) {
      $fID = Config::get('farmmarket.last-import-id');
      $delimiter = $this->getDelimiter(Config::get('farmmarket.last-import-delimiter'));
      $enclosure = $this->getEnclosure(Config::get('farmmarket.last-import-enclosure'));
      $import_count = 0;
      $fi = new FileImporter();
      $f = File::getByID($fID);
      if (!is_object($f)) {
        $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
      }
      else {
        $fsr = $f->getFileResource();
        if (!$fsr->isFile()) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        } else {
          $rows = $this->parseFile($fsr, $delimiter, $enclosure);
          if ($this->deleteRecords())
            $import_count = $this->createRecords($rows);
        }
      }
      if ($this->error->has()) {
        $this->setFormData();
        $this->view();
      }
      else {
        $this->redirect('/dashboard/market/vendors', 'view', "Imported $import_count vendors successfully!");
      }
    }
    else {
      $this->error->add($this->token->getErrorMessage());
      $this->view();
    }
  }

  public function view() {
    $this->setFormData();
  }

  private function parseFile($fsr, $delimiter, $enclosure = null) {
    error_log("Delimiter: $delimiter, Enclosure: $enclosure");
    $rows = $rows = array_filter(preg_split("/\r\n|\n|\r/", $fsr->read()));
    $header = ['name','phone','email','keywords'];
    $data = array();
    foreach ($rows as $row) {
      $row = str_getcsv($row, $delimiter, $enclosure);
      if (count($row) == count($header))
        $data[] = array_combine($header, $row);
    }
    return $data;
  }

  private function deleteRecords() {
    $vendor = new Vendor();
    $ok = $vendor->DeleteAll();
    error_log(print_r($ok, true));
    if (!is_object($ok)) {
      $this->error->add('Unable to delete existing vendor data.');
      return false;
    }
    return true;
  }

  private function createRecords($rows) {
    $count = 0;
    foreach ($rows as $row) {
      $vendor = new Vendor();
      $vendor->name = trim($row['name']);
      $vendor->email = trim($row['email']);
      $vendor->phone = trim($row['phone']);
      $vendor->keywords = trim($row['keywords']);
      $vendor->save();
      $count++;
    }
    return $count;
  }

  private function setFormData() {
    $this->set('fID', Config::get('farmmarket.last-import-id'));
    $this->set('delimiter', Config::get('farmmarket.last-import-delimiter'));
    $this->set('enclosure', Config::get('farmmarket.last-import-enclosure'));
  }

  private function getDelimiter($param) {
    if ($param == 'comma')
      return ",";
    elseif ($param == 'tab') 
      return "\t";
    else
      $this->error->add('Invalid delimter selected.');
  }

  private function getEnclosure($param) {
    if ($param == 'double-quoute')
      return '"';
    elseif ($param == 'single-quoute')
      return "'";
    else
      return null;
  }

}