<?php
namespace Concrete\Package\FarmersMarket\Controller\SinglePage\Dashboard\Market;

use \Concrete\Core\Page\Controller\DashboardPageController;
use Loader;
use \Concrete\Package\FarmersMarket\Src\Models\Vendor;

class AddVendor extends DashboardPageController {
  
  public $helpers = array('html', 'form');

  public function edit($vID = null) {
    // Get vendor by ID
    $vID = $this->post('id') != null ? $this->post('id') : $vID;
    $vendor = new Vendor();
    if ($this->isPost()) {
      // Update vendor
      if ($vID)
        $vendor->id = $vID;
      $vendor->name = trim($this->post('name'));
      $vendor->email = trim($this->post('email'));
      $vendor->phone = trim($this->post('phone'));
      $vendor->keywords = trim($this->post('keywords'));
      $this->error = $vendor->validate();

      if (!$this->error->has()) {
        $ok = $vendor->save();
        if (!$ok) {
          $dbErrMsg = Loader::db()->ErrorMsg(); // Must call this before Config::get(), otherwise ErrorMsg() returns empty!
          if (Config::get('SITE_DEBUG_LEVEL') > 0) {
            $this->error->add("Database Error: The vendor could not be saved for the following reason: " . $dbErrMsg);
          } else {
            $this->error->add("Error: Sorry, but there was an error with the system and the vendor could not be saved at this time.");
          }
        }
        else {
          $this->set('vendor', $vendor);
          $this->set('message', 'Vendor saved successfully');
        }
      }
    }
    else {
      $item = $vendor->FindByID($vID);  
      if (is_object($item)) {
        $this->set('vendor', $item);
      }
      else {
        $this->redirect('/dashboard/market/add_vendor');
      }
    }

  }
}