<?php
namespace Concrete\Package\FarmersMarket\Controller\SinglePage\Dashboard\Market;

use \Concrete\Core\Page\Controller\DashboardPageController;
use \Concrete\Package\FarmersMarket\Src\Models\Vendor;

class Vendors extends DashboardPageController {
  public $helpers = array('html', 'form');

  public function view($msg = null) {
    $vendor_list = $this->getVendors();
    $this->set('vendors', $vendor_list);
    $this->set('record_count', count($vendor_list));
    if ($msg)
      $this->set('success', $msg);
  }

  public function delete_check($vID, $name) {
    $this->set('remove_name', urldecode($name));
    $this->set('remove_id', $vID);
    $this->view();
  }

  public function delete($vID, $name) {
    $vendor = new Vendor();
    $item = $vendor->FindByID($vID);
    if (is_object($item)) {
      $item->Delete();
      $this->set('remove_name', '');
      $this->set('remove_cid', '');
    }
    else {
      $this->error->add("Could not delete vendor, ID not found.");
    }
    $this->view(t('"' . urldecode($name) . '" has been deleted'));
  }

  private function getVendors() {
    $vendors = new Vendor();
    return $vendors->Find('1=1');
  }
}