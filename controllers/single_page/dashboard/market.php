<?php
namespace Concrete\Package\FarmersMarket\Controller\SinglePage\Dashboard;

use \Concrete\Core\Page\Controller\DashboardPageController;
use Loader;

class Market extends DashboardPageController
{
  public function view()
  {
    $this->redirect('/dashboard/market/vendors');
  }
}