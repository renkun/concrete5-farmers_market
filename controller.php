<?php
namespace Concrete\Package\FarmersMarket;

use App;
use Core;
use Package;
use Loader;
use Page;
use \Concrete\Core\Page\Single as SinglePage;
use \Concrete\Core\Block\BlockType\BlockType;

defined('C5_EXECUTE') or die(_('Access Denied'));

class Controller extends Package {
  protected $pkgHandle = 'farmers_market';
  protected $pkgName = 'DDA Farmers Market';
  protected $pkgDescription = 'Manage and display farmers market data.';
  protected $appVersionRequired = '5.7.5';
  protected $pkgVersion = '1.0.0';

  public function install() {
    $pkg = parent::install();
    $this->install_or_upgrade($pkg);
  }

  public function upgrade() {
    $pkg = Package::getByHandle($this->pkgHandle);
    $this->install_or_upgrade($pkg);
    parent::upgrade();
  }

  // Install all single pages and blocks
  private function install_or_upgrade($pkg) {
    // Blocks
    $this->getOrInstallBlockType($pkg, 'vendor_search');
    // Dashboard pages
    $this->getOrAddSinglePage($pkg, '/dashboard/market', 'Farmers Market');
    $this->getOrAddSinglePage($pkg, '/dashboard/market/vendors', 'Vendors');
    $this->getOrAddSinglePage($pkg, '/dashboard/market/add_vendor', 'Add Vendor');
    $this->getOrAddSinglePage($pkg, '/dashboard/market/import_vendors', 'Import Vendors');
  }

  private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
    $sp = SinglePage::add($cPath, $pkg);
    if (is_null($sp)) {
      //SinglePage::add() returns null if page already exists
      $sp = Page::getByPath($cPath);
    } else {
      //Set page title and/or description...
      $data = array();
      if (!empty($cName)) {
        $data['cName'] = $cName;
      }
      if (!empty($cDescription)) {
        $data['cDescription'] = $cDescription;
      }
      if (!empty($data)) {
        $sp->update($data);
      }
    }
    return $sp;
  }

  private function getOrInstallBlockType($pkg, $btHandle) {
    $bt = BlockType::getByHandle($btHandle);
    if (empty($bt)) {
      BlockType::installBlockTypeFromPackage($btHandle, $pkg);
      $bt = BlockType::getByHandle($btHandle);
    }
    return $bt;
  }

}