<?php
namespace Concrete\Package\FarmersMarket\Src\Models;
use \Concrete\Core\Legacy\Model;
use Loader;

class Vendor extends Model {
  var $_table = 'FarmersMarketVendors';

  // Cleaner than Find, which returns an array
  public function FindByID($vID) {
    $item = $this->Find("id=?", array($vID));  
    if (is_object($item[0])) {
      return $item[0];
    }
  }

  public function validate() {
    // Due to some strange behavior in ADODB-active-record, this validation function will not work properly
    // unless all of the record's values have been set or initialized to null.
    $error = Loader::helper('validation/error');
    if (!$this->name) {
      $error->add(t('You must enter a name.'));
    }
    if (strlen($this->name) > 75) {
      $error->add(t('Name is too long. Name is limited to 75 characters.'));
    }
    // if (!$this->email) {
    //   $error->add(t('You must enter your email address.'));
    // } 
    if ($this->email && !$this->validate_email_format()) {
      $error->add(t('Email address is not in a valid format -- please check that you entered it correctly.'));
    }
    if (strlen($this->email) > 75) {
      $error->add(t('Email is too long. Email is limited to 75 characters.'));
    }
    // if (!$this->phone) {
    //   $error->add(t('You must enter a phone number.'));
    // }
    if (strlen($this->phone) > 35) {
      $error->add(t('Phone is too long. Phone is limited to 15 characters.'));
    }
    if (!$this->keywords) {
      $error->add(t('You must enter a keyword list.'));
    }
    if (strlen($this->keywords) > 1200) {
      $error->add(t('Keywords string is too long. Keywords is limited to 1200 characters.'));
    }
    return $error;
  }

  // The Delete method in parent appears to just delete every record?? We'll override that one.
  public function Delete() {
    if ($this->id) {
      $db = Loader::db();
      $db->Execute("DELETE from ".$this->_table." where id = ?", array($this->id));
    }
  }

  public function DeleteAll() {
    $db = Loader::db();
    $result = $db->Execute("TRUNCATE TABLE ".$this->_table);
    return $result;
  }

  private function validate_email_format() {
    $regex = "/^\S+@\S+\.\S+$/";
    return (bool)preg_match($regex, $this->email);
  }

}