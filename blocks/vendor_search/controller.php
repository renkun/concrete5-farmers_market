<?php  
namespace Concrete\Package\FarmersMarket\Block\VendorSearch;

use \Concrete\Core\Block\BlockController;
use Loader;
use Page;

class Controller extends BlockController {

  protected $btTable = 'btMarketVendorSearch';
  protected $btDefaultSet = "forms";
  protected $btInterfaceWidth = "400";
  protected $btInterfaceHeight = "430";
  protected $btExportPageColumns = array('postTo_cID');

  public $postTo_cID = "";
  public $showEmail = 1;
  public $showPhone = 1;
  public $showKeywords = null;

  public function getBlockTypeDescription() {
    return t("Search Farmers Market vendor records");
  }

  public function getBlockTypeName() {
    return t("Farmers Market Search");
  }

  public function view() {
    $this->set('label', $this->label);
    $c = Page::getCurrentPage();
    if ($this->postTo_cID != 0) {
      $resultsPage = Page::getById($this->postTo_cID);
      $this->set('resultTargetURL', $resultsPage->cPath);
    }
    else {
      $this->set('resultTargetURL', $c->getCollectionPath());
    }

    if (!empty($_REQUEST['query'])) {
      $db = Loader::db();
      $query = '%'.$_REQUEST['query'].'%';
      $results = $db->GetAll('SELECT * FROM FarmersMarketVendors WHERE name LIKE ? OR email LIKE ? OR phone LIKE ? OR keywords LIKE ?', array($query,$query,$query,$query));
      if (!empty($results)) {
        $this->set('results', $results);
        $this->set('query', $_REQUEST['query']);
      }
      else {
        $this->set('error', 'No results found.');
      }
    }
  }

  public function save($data) {
    $data += array(
      'label' => '',
      'columns' => 1,
      'showEmail' => 1,
      'showPhone' => 1,
      'showKeywords' => 0,
      'postTo_cID' => 0,
      'externalTarget' => 0,
    );
    $args = array();
    $args['label'] = trim($data['label']);
    $args['columns'] = intval($data['columns']);
    $args['showEmail'] = intval($data['showEmail']);
    $args['showPhone'] = intval($data['showPhone']);
    $args['showKeywords'] = intval($data['showKeywords']);
    if ($data['externalTarget'] && $data['postTo_cID'] > 0) {
      $args['postTo_cID'] = $data['postTo_cID'];
    }
    else {
     $args['postTo_cID'] = 0; 
    }
    parent::save($args);
  }
}