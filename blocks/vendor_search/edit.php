<?php
defined('C5_EXECUTE') or die("Access Denied.");
$form = Loader::helper('form');
/**
 * Post to another page, get page object.
 */
$basePostPage = NULL;
if (isset($postTo_cID) && intval($postTo_cID) > 0) {
  $basePostPage = Page::getById($postTo_cID);
}
/**
 * Verify object.
 */
if (is_object($basePostPage) && $basePostPage->isError()) {
  $basePostPage = NULL;
}
$select_page = Loader::helper('form/page_selector');
?>

<fieldset>
  <div class='form-group'>
    <label for='label'><?php echo t('Search Button Label')?>:</label>
    <?php echo $form->text('label', $label); ?>
  </div>
  <div class='form-group'>
    <label for='label'><?php echo t('Display Fields')?>:</label>
    <div class="checkbox">
      <label><?php echo $form->checkbox('showEmail', 1, $showEmail); ?>Email</label><br/>
      <label><?php echo $form->checkbox('showPhone', 1, $showPhone); ?>Phone</label><br/>
      <label><?php echo $form->checkbox('showKeywords', 1, $showKeywords); ?>Keywords</label>
    </div>
  </div>
  <div class="form-group">
    <label for='label'><?php echo t('Results Columns')?>:</label>
    <select class="form-control" name="columns">
      <option value="1"<?php if ($columns == 1) { ?> selected<?php } ?>>1</option>
      <option value="2"<?php if ($columns == 2 || $columns == '') { ?> selected<?php } ?>>2</option>
      <option value="3"<?php if ($columns == 3) { ?> selected<?php } ?>>3</option>
    </select>
  </div>
  <div class='form-group'>
    <label for='title' style="margin-bottom: 0px;"><?php echo t('Results Page')?>:</label>
    <div class="checkbox">
        <label for="ccm-searchBlock-externalTarget">
            <input id="ccm-searchBlock-externalTarget" name="externalTarget" type="checkbox" value="1" <?php echo ($basePostPage !== NULL)?'checked':''?> />
            <?php echo t('Post Results to a Different Page')?>
        </label>
    </div>
    <div id="ccm-searchBlock-resultsURL-wrap" class="input" style=" <?php echo (strlen($basePostPage !== NULL)) ? '' : 'display:none' ?>" >
        <?php
        if ($basePostPage !== NULL) {
            print $select_page->selectPage('postTo_cID', $basePostPage->getCollectionID());
        } else {
            print $select_page->selectPage('postTo_cID');
        }
        ?>
    </div>
  </div>
</fieldset>