<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<form action="<?php echo $view->url($resultTargetURL)?>" method="get" class="ccm-search-block-form" id="merch-search-form">
  <input name="query" type="text" value="<?php echo htmlentities($query, ENT_COMPAT, APP_CHARSET)?>" class="ccm-search-block-text" id="query" />
  <input name="submit" type="submit" value="<?php echo h($label)?>" class="btn btn-default ccm-search-block-submit" />
</form>

<?php if ($results): 
  $col_class= "col-xs-12";
  if ($columns == 2)
    $col_class = "col-xs-6";
  if ($columns == 3)
    $col_class = "col-xs-6 col-sm-4";
?>
<style>
  .vendor-search-results {
    margin-top: 60px;
  }
  .vendor-search-results h5 {
    font-size: 28px;
    text-align: left;
  }
  .vendor-search-results .result {
    margin-bottom: 35px;
  }
  .vendor-search-results .result > div {
    width: 100%;
    min-height: 35px;
    margin-bottom: 10px;
    list-style: none;
    background-position: left center;
    background-repeat: no-repeat;
    background-size: 30px;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -ms-flex-align: center;
    -webkit-align-items: center;
    -webkit-box-align: center;
    align-items: center;
  }
  .vendor-search-results .result > div.phone {
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAElklEQVRYw82Ya0xTZxjHfz0F6yZ4QaDAXNXgQGe04FyADm+4MLMUp5AlgnbC4uaIm5upurAtW6LJsi3Wy6Izuxgv1YnOqMjinKiZH5xj2UDBGkFdFEVFruVWqNDuw2kZFEp75Jjs/63P++97fnnO+77neV4FUmQwKoE5wHxgJhADjAFGAVagEagE/gLOAb9hNnX7O73CT4hwYA2QDURIwH8A7AG2YDY9HBqMwTgMWA/kAU9LymJftQNfAF9iNtmlwxiMMcAhIG4IEJ66BCzBbKrwH8ZgnAMcB0bLCOJWE7AIs+m8bxiDMQUoZGivxZdsgB6z6Zx3GIMxFihG3B1PWlYgofcrE3qBqIB8byAJ0Rqqt33K9Gcj5YIZBRxyPdcDBtbiZbEmRGswZS1Ev3kXO7MzGKEaJheQFljn/qFwZUUN/MMA62RCaAj7c7NYvHUPtS2tvDZjKmnxU1mx67BcQDZgImZTjTsz7+NlwW7IeIX39h2jtqUVgIISCzb7IzKT4uWCeQr4AEDAYAwAcrw5NWNHU3q7uk9s7cFCclN0RIePlQsoB4MxQABmM8gR73A6+8U6u7r46KeTbFu2SC4YNTBPQPzoedXwwMB+saDhKj5//VU2FhTJBQPwsgC8MJjjXqOVcSF9d/vq1GR2nLlA8c0qOWG0AjB5MMcP5/8kN0XXJxanieLs1etyggDECoj1iFedLq8gOXYikaNH9sSq6puIjQiTGyZEAEYO5nA4nazP/5ktSxf2xA78XsKKuQlyw4wUgGZfruKbVVTVN/H2vEQASm9XM0I1DN1zE+SEaRYQS0Wfyjt8kowXp5M4aTwAq/YeY1NmGpPUoXLBNAjANX+c3Q4Hy3b+yKbMNKZEqaltaeWNbw+yb2UmLw2QoYRoDafWvSUFtkKJVjcFmOWPu91up+hKJQdyl/JreQW36ho4UXKVzxanoo+bQn1rOzZ7F+kzp7FmwWw+PnKKHcvTedTdTfmd+76mP6pwFVNnpeRzkjqU/e9kkf1dPtfui3V2nCaKLN0MIkYFY6l+wPaiC7R12lEFBPDVEj3Bw1Ws3n+c1o5Ob9OmKlzfpruIR7LfGh86hgO5S1l7sJA/btz26U+Lf54P9Sm8u/col6rueQ7XAOOUlF10oNWNBZKlwFjbOzhRauFrwyJGqFT8fevuoP7KB7WcvlJBXtp8CkosnsPbMZuK3CXEVsS6QpLqWtrQb97FxLAQ8lctI2qM9yMrLDiIyZHh5Hx/yHPI5no+SgDKLrah1SmAFKlADqeTM5br3G208s3yDGIiwrhRU4fV1tHjiR//DPtWZlJQYuFeY79jbQNm0y/QuyAXa9FixFLwsSQoFKROiyV71kzCgoNwOp0EBii5U9/EJ0dOcauuwfMvlxGL8s6+MCLQ/6Q7AFwD6UCHtHklqwNI9+wshX42sbFagNj5PQk1AQs8G7iBYUSg80ASUCYzyGUgaaDWFty7aSCVXaxDq9sN2IFEIJDHlw3YCORgNtV4M/l7P6NGbCfeBMIlQDwEdiPez9T4MvsH8x+UEpiLeB65b65CEAu0ZqCBIdxc/Qu2jG6xVexO0wAAAABJRU5ErkJggg==');
  }
  .vendor-search-results .result > div.email {
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAADFUlEQVRYw83YT2gcZRjH8c/O7rZJJN00hyaCOaRCWlRMqQZtrUZjwAiBlnrpZaFREMFDlVVphF4UMSKLBz14kRTWgz0IUQsK2tCKUEStTSiaCkZKLTRF0iTSJFZtPcykJttsd5PNv99t5v3N+3znnXfe93nemIUonYmjFY/jfjRhI1IYx2X8gu/Rj+Ny2X9L7T5WIsQmvIj9qF8A/kUcxjty2UvlwaQz6/AKulG1oFGcq0n04C257NWFw6QzTTiCbWVA5Os09sllz5YOk860og81SwgyozHskcueKA6TzrThM+V9lmKaQqdctr8wTDqzBd8K/47l1jgemP3Jglkg6/HRCoGI4hyJ4ubB8JKlnaylqBkvz1zEolGpw7C8efJ+11P2bL/H9N//lB21IpnQd+qM53o/zm+aQqNcdiQR3ThgnglbkUw68OEnnmzeWjbMF4NDOu6dt59KvIDuhHQmga5CndxeU60+Va3naL/F6mBnm/pU9a0sXdKZQwk8osgSf3H8T8d//nXRMPsfbilmqcNjgXDTWwtqD3DfalNEag5Q/uxcGm0JhPnIWlBtgA2rTRFpQ4CJ1aaINBEIU8W1oNEAQ6tNEelsAj/giUKO69jV1Ojws/sWHWVXU6Mfz10oZhtI4BhezW9p2dygPlUtGY9778tvynrl0+cuSMbj6lPVWjY3+G74/Hy2r2LR3vS7cEm+oakPehw9/ZMrfxXMn22sqtS69U59p87Yvf1uXw8Nuzw5VdB/2/p1OrfdpfKZg/lNI7hjJoV4E3Mcz7c/5NDu9lumD0EQ01BbY3xyWqqqwvnRMdeuXS/or0gmvPHpMe/ePNI9ctnu2fnMb8LtfKV1I5+Jg8GTVzTvjKFtFWBek8t+zty0820MrDDIQBQXa7Y6QNSwF9PLDDKNvfmVZXCTLSysOoSV33JoDB35Bdz8MCHQCezA4BKDDGDHfKUtxAs+NnjyD807e3EVDyJZBsQUXkeXXHakkKnU85k6YTnxNDYtAOISeoXnMyPFzKXB/A8Vx6PC9Wjm5KpWmKBNYFQZJ1f/AZxp0PAzONzMAAAAAElFTkSuQmCC');
  }
  .vendor-search-results .result > div.keywords {
    font-size: 80%;
  }
  .vendor-search-results .result > div > span {
    margin-left: 40px;
  }
</style>
<div class="vendor-search-results">
  <div class="row">
  <?php
    $count = 0;
    foreach ($results as $result) { 
      if ($count % $columns == 0) { ?>
  </div>
  <div class="row">
  <?php }
      $count++;
  ?>
    <div class="<?php echo $col_class ?> result">
      <h5><?php echo $result['name']; ?></h5>
      <?php if ($showEmail && $result['email']) { ?>
      <div class="email"><span><a href="mailto:<?php echo $result['email']; ?>"><?php echo $result['email']; ?></a></span></div>
      <?php } ?>
      <?php if ($showPhone && $result['phone']) { ?>
      <div class="phone"><span><?php echo $result['phone']; ?></span></div>
      <?php } ?>
      <?php if ($showKeywords && $result['keywords']) { ?>
      <div class="keywords"><?php echo $result['keywords']; ?></div>
      <?php } ?>
    </div>
  <?php } ?>
  </div>
</div>
<?php endif; ?>